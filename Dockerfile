FROM gitlab-registry.cern.ch/etf/docker/etf-exp:qa

LABEL maintainer="Marian Babik <Marian.Babik@cern.ch>"
LABEL description="WLCG ETF ATLAS"
LABEL version="1.0"

ENV NSTREAM_ENABLED=0

# Middleware
RUN yum -y update
RUN yum -y install yum-priorities
RUN yum -y install https://repo.opensciencegrid.org/osg/3.6/osg-3.6-el7-release-latest.rpm
# FIX: gfal2 (osg override)
# RUN cd /etc/yum.repos.d && wget http://dmc-repo.web.cern.ch/dmc-repo/dmc-el7.repo
# RUN sed "7i priority=97" -i /etc/yum.repos.d/dmc-el7.repo
RUN sed "7i priority=99" -i /etc/yum.repos.d/epel.repo
# FIX: gfal2 (osg override)
# RUN cd /etc/yum.repos.d && wget http://dmc-repo.web.cern.ch/dmc-repo/dmc-el7.repo
# RUN sed "7i priority=97" -i /etc/yum.repos.d/dmc-el7.repo
# RUN yum clean all

# FIX: oidc-agent (epel override)
# RUN cd /etc/yum.repos.d && wget https://repo.data.kit.edu/data-kit-edu-centos7.repo
# RUN sed "7i priority=97" -i /etc/yum.repos.d/data-kit-edu-centos7.repo

# FIX: condor client (osg override)
# RUN yum -y install https://research.cs.wisc.edu/htcondor/repo/9.0/htcondor-release-current.el7.noarch.rpm

# Core deps
RUN yum -y install voms voms-clients-java oidc-agent-cli

# Condor client
RUN yum -y install condor condor-python
# HTCondor-CE client
RUN yum -y install htcondor-ce-client
COPY ./docker/config/ce-client.conf /etc/condor-ce/config.d/ce-client.conf

# Xroot
RUN yum -y install xrootd-python xrootd-client xrootd-libs xrootd-client-libs

# SRM
RUN yum -y install python2-gfal2 python2-gfal2-util gfal2-plugin-srm gfal2-plugin-gridftp gfal2-plugin-http gfal2-plugin-sftp

# Python 3 (gfal2/python-nap)
RUN yum -y install python3-gfal2 python3-nap python36-pyOpenSSL
RUN yum -y install --disablerepo=osg gfal2-plugin-xrootd

# ARC
RUN rpm -ivh https://download.nordugrid.org/packages/nordugrid-release/releases/6/centos/el7/x86_64/nordugrid-release-6-1.el7.noarch.rpm
RUN yum -y install nordugrid-arc-client nordugrid-arc-plugins-needed nordugrid-arc-plugins-globus
# ARC config
RUN mkdir /opt/omd/sites/$CHECK_MK_SITE/.arc
RUN echo "joblisttype=XML" > /opt/omd/sites/$CHECK_MK_SITE/.arc/client.conf
RUN chown -R $CHECK_MK_SITE /opt/omd/sites/$CHECK_MK_SITE/.arc/

# MW env
COPY docker/config/grid-env.sh /etc/profile.d/
RUN echo "source /etc/profile.d/grid-env.sh" >> /opt/omd/sites/$CHECK_MK_SITE/.profile
# IAM VOMSES
RUN mkdir -p /etc/vomses
COPY docker/config/voms-atlas-auth.app.cern.ch.vomses /etc/vomses

# ETF base plugins
RUN yum -y install python-pip
RUN pip install pexpect ptyprocess argparse
RUN yum -y install --nogpgcheck nagios-plugins-globus nagios-plugins-tokens nagios-plugins nagios-plugins-webdav \
                                python-jess python-wnfm pytz logrotate python-GridMon

# ETF WN-qFM payload
RUN mkdir -p /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
RUN mkdir -p /usr/libexec/grid-monitoring/wnfm/bin
RUN cp /usr/bin/etf_wnfm /usr/libexec/grid-monitoring/wnfm/bin/
RUN cp -r /usr/lib/python2.7/site-packages/pexpect /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
RUN cp -r /usr/lib/python2.7/site-packages/ptyprocess /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
RUN cp -r /usr/lib/python2.7/site-packages/wnfm /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
RUN cp /usr/lib/python2.7/site-packages/argparse.py /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages/

# ETF streaming
RUN mkdir -p /var/spool/nstream/outgoing && chmod 777 /var/spool/nstream/outgoing
RUN mkdir /etc/stompclt
COPY docker/config/ocsp_handler.cfg /etc/nstream/

# ATLAS config and payload
COPY extras/vofeed_atlas.py /usr/lib/ncgx/x_plugins/
COPY extras/getCRICATLASInfo.sh /etc/cron.daily/
RUN mkdir -p /usr/libexec/grid-monitoring/probes/org.atlas/wnjob/
COPY src/wnjob /usr/libexec/grid-monitoring/probes/org.atlas/wnjob
COPY src/DDM /usr/libexec/grid-monitoring/probes/org.atlas/DDM
COPY extras/getCRICATLASInfo.py /usr/libexec/grid-monitoring/probes/org.atlas/
# ATLAS DDM probe logrotate
COPY extras/gfal2_logs /etc/logrotate.d/
RUN chmod 0644 /etc/logrotate.d/gfal2_logs

# ETF config
COPY extras/wlcg_atlas.cfg /etc/ncgx/metrics.d/
COPY docker/config/atlas_checks.cfg /etc/ncgx/conf.d/
COPY docker/config/ncgx.cfg /etc/ncgx/

EXPOSE 443 6557
COPY docker/docker-entrypoint.sh /
ENTRYPOINT /docker-entrypoint.sh
